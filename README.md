# 50 Tips Of Best Practices for Unity

derived from http://www.gamasutra.com/blogs/HermanTulleken/20160812/279100/50_Tips_and_Best_Practices_for_Unity_2016_Edition.php .

日本語訳

## Unity のための50のベストプラクティス

## Workflow

1. すべてのオブジェクトを同じ単位で測ること

2. すべてのシーンを実行可能にしておくこと

3. ソースコード管理システムを効果的に使おう

- Asset はテキスト形式にシリアライズする
(よりマージしやすくなるため)

- シーンとプレハブの共有は戦略的に行う

- タグをブックマークとして使う

- ブランチを切るときは厳密に切り分ける

5. Unityのバージョンを上げるときはチームで同時に行おう

6. サードパーティのセットをインポート、または自分のプロジェクトからエクスポートするときはクリーンなプロジェクトから行おう

7. ビルドは自動化しよう

http://www.gamasutra.com/blogs/EnriqueJGil/20160808/278440/Unity_Builds_Scripting_Basic_and_advanced_possibilities.php

8. セットアップの手法はドキュメント化しよう

## Coding

9. すべてのコードは namespace で切り分ける。

10. `Assertion` を使おう

11. string(文字列)をゲーム画面への表示以外に使わない。

12. `SendMessage` と `Invoke` は使わない。

13.

```cs
public static class TransformExtensions
{
   public static void SetX(this Transform transform, float x)
   {
      Vector3 newPosition =
         new Vector3(x, transform.position.y, transform.position.z);

      transform.position = newPosition;
   }
   ...
}
```